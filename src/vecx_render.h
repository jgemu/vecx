#ifndef VECX_RENDER_H
#define VECX_RENDER_H

#define VWIDTH 330
#define VHEIGHT 410

void vecx_render(void);
void vecx_render_set_buf(uint32_t*);
void vecx_render_set_algo(int);
void vecx_render_set_scale(int);

#endif
