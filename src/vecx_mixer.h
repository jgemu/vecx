#ifndef VECX_MIXER_H
#define VECX_MIXER_H

#include "ay38910.h"

void vecx_mixer_deinit(void);
void vecx_mixer_init(void);

void vecx_mixer_set_buffer(int16_t*);
void vecx_mixer_set_callback(void (*)(size_t));
void vecx_mixer_set_psg(ay38910_t*);
void vecx_mixer_set_rate(size_t);
void vecx_mixer_set_rsqual(uint8_t);
void vecx_mixer_wr_dac(int16_t);
void vecx_mixer_resamp(size_t);

#endif
