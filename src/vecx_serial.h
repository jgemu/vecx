#ifndef VECX_SERIAL_H
#define VECX_SERIAL_H

void vecx_serial_begin(void);
void vecx_serial_pushblk(uint8_t*, uint8_t*, size_t);
void vecx_serial_popblk(uint8_t*, uint8_t*, size_t);
void vecx_serial_push8(uint8_t*, uint8_t);
void vecx_serial_push16(uint8_t*, uint16_t);
void vecx_serial_push32(uint8_t*, uint32_t);
uint8_t vecx_serial_pop8(uint8_t*);
uint16_t vecx_serial_pop16(uint8_t*);
uint32_t vecx_serial_pop32(uint8_t*);
size_t vecx_serial_size(void);

#endif
